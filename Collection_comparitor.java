package week6.MenuDriven_Comparetor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Student{
    int ronim;
    String name;
    String city;

    public Student(int ronim, String name, String city) {
        this.ronim = ronim;
        this.name = name;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Student{" +
                "ronim=" + ronim +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}

class SortByRoll implements Comparator<Student>{

    @Override
    public int compare(Student o1, Student o2) {
        return o1.ronim-o2.ronim;
    }
}
class SortByName implements Comparator<Student>{

    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);
    }
}

class SortByCity implements Comparator<Student>{

    @Override
    public int compare(Student o1, Student o2) {
        return o1.city.compareTo(o2.city);
    }
}

public class Collection_comparitor {
    static ArrayList<Student> arrayList = Collection_Menu_driven.arrayList;
    static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
//    public static void addData() throws IOException {

//    }

    public static void beforeSort(){
        System.out.println("********************brfore***********************");
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
        }

public static void rollnum() {
    System.out.println("********************afterRoll**************************");
    Collections.sort(arrayList, new SortByRoll());
    for (int i = 0; i < arrayList.size(); i++) {
        System.out.println(arrayList.get(i));
    }
}
public static void namee() {
        System.out.println("********************afterName**************************");
        Collections.sort(arrayList, new SortByName());
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));

        }
    }
    public static void cityy(){
        System.out.println("********************afterCity**************************");
        Collections.sort(arrayList,new SortByCity());
        for (int i=0;i<arrayList.size();i++){
            System.out.println(arrayList.get(i));
        }

    }
}
