package week6.MenuDriven_Comparetor;

import week6.MenuDriven_Comparetor.Collection_comparitor;
import week6.MenuDriven_Comparetor.Student;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Collection_Menu_driven {
    static ArrayList<Student> arrayList = new ArrayList<>();

    static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    public static void menu() throws IOException {
//        System.out.println("enter 0 for enter the data");
        System.out.println("enter 1 for before sorting data");
        System.out.println("enter 2 for after sorting data by rollnumber");
        System.out.println("enter 3 for after sorting data by name");
        System.out.println("enter 4 for after sorting data by city");
        System.out.println(" enter 5 to exit");
        System.out.println("enter the option");
        int option=Integer.parseInt(bufferedReader.readLine());
switch (option){
//    case  0:
//        new Collection_comparitor().addData();
    case 1:
        new Collection_comparitor().beforeSort();
        break;
    case 2:
        new Collection_comparitor().rollnum();
        break;
    case 3:
        new Collection_comparitor().namee();
        break;
    case 4:
        new Collection_comparitor().cityy();
        break;
    case 5:
        System.exit(0);
        break;
    default:
        System.out.println("wrong input");

}

    }

    public static void main(String[] args) throws IOException {
        System.out.println("Enter the number of students");
        int numberofStudent=Integer.parseInt(bufferedReader.readLine());
        System.out.println("enter the roll_number,name,city of students");
        for (int i = 0; i <numberofStudent ; i++) {
            int rollnim=Integer.parseInt(bufferedReader.readLine());
            String name=bufferedReader.readLine();
            String city=bufferedReader.readLine();
            arrayList.add(new Student(rollnim,name,city));

        }

        while (true){
            menu();
        }
    }
}
